package com.ks.ejemplos;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        escribe escritura = new escribe();
        comida china = new comida();
        comida mexicana = new comida();

        china.setTipo("china");
        china.setComida("credo agridulce");

        escritura.setAlimento(china);
        escritura.ahora();

        mexicana.setTipo("mexicana");
        mexicana.setComida("mole");

        escritura.setAlimento(mexicana);
        escritura.ahora();
    }
}
